core = 6.x
api = 2

; Contrib modules
projects[captcha][version] = "2.4"
projects[recaptcha][version] = "1.7"
projects[honeypot][version] = "1.13"
